# Object Detection

This repository contains RST files for generating documentation.

**Documentation online**: [https://inr-documentation.readthedocs.io/](https://inr-documentation.readthedocs.io/en/latest/index.html)


## Documentation

Run these commands to generate the documentation:

```
cd docs
make html          # create html documentation
make latexpdf      # create a PDF file
```

Run these commands to show the documentation in your local machine:

```
cd docs/_build/html
python -m http.server
```

## Contact

Auraham Camacho `auraham.cg@gmail.com`



