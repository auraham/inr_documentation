Descripción de las imágenes médicas y el formato DICOM
======================================================

**Resumen**

-  Las imágenes médicas siguen el formato DICOM (`Digital Imaging and Communications in Medicine`).
-  El formato DICOM se compone de metadatos y de una matriz numérica.
-  Una tomografía es una secuencia de ``D`` imágenes.
-   Las dimensiones de una tomografía son ``(C, D, H, W)`` o ``(Channels, Depth, Height, Width)``. Una tomografia de ``(1, 99, 512, 512)`` es una secuencia de ``99`` imágenes de ``512x512`` de un solo canal (escala de grises).
-  Podemos utilizar la librería ``pydicom`` para leer archivos DICOM.


Formato DICOM
-------------

Las imágenes médicas suelen estar en el formato **DICOM** (`Digital Imaging and Communications in Medicine`). Este formato se compone de dos partes principales: metadatos y una matriz numérica.
Los metadatos contienen información sobre el paciente y algunos atributos de la imagen, como su tamaño (i.e., número de filas y columnas). Por otro lado, la matriz representa la imagen médica como tal.
    
Las imágenes facilitadas por el INR están en el formato DICOM y son en su mayoría tomografías. 
Una tomografía se compone de una secuencia de ``D`` imágenes. 
Cada imagen en esta secuencia se le suele llamar corte. Para leer una tomografía en una computadora, es necesario leer las ``D`` imágenes de la secuencia en el orden correcto.

Por ejemplo, una  tomografía suele estar organizada de la siguiente manera:

.. code-block:: none

    volumetric-dicom/
        000000.dcm
        000001.dcm
        ...
        000098.dcm

en donde ``volumetric-dicom`` es un directorio, mientras que ``000000.dcm-000098.dcm`` son 99 archivos en formato DICOM. 

Cada archivo DICOM en el directorio ``volumetric-dicom`` representa un corte de la tomografía.

A pesar de que los archivos están numerados en el directorio, es recomendable utilizar el atributo ``SliceLocation`` para establecer el orden de los cortes, ya que el orden de los archivos en el directorio no es necesariamente el mismo que el orden de los cortes en la secuencia. 

Cada corte se trata de una imagen que está representada como una matriz de tamano ``(H, W)``, donde ``H`` es el número de filas y ``W`` es el número de columnas.
Para definir la posición del corte en la secuencia de imágenes, es necesario acceder al atributo ``SliceLocation``.
Una vez que se han leído todos los archivos DICOM y se ha definido el orden de los cortes, se pueden combinar todos los cortes en un tensor. Un tensor es la generalización de una matriz en más de dos dimensiones.
También suele emplearse el término volumen para referirse al tensor.

Para leer un archivo DICOM con Python, podemos utilizar las siguientes librerías:

- imageio `https://imageio.readthedocs.io/en/stable/ <https://imageio.readthedocs.io/en/stable/>`_.
- pydicom  `https://pydicom.github.io/pydicom/stable/index.html <https://pydicom.github.io/pydicom/stable/index.html>`_. 

La librería imageio permite leer todos los archivos DICOM en un directorio y generar un volumen utilizando la función ``volread()``. El siguiente ejemplo muestra cómo crear un volumen a partir de las imágenes DICOM del directorio ``dir_path``:
        
.. code-block:: python

    import imageio
    dir_path = "/media/data/datasets/volumetric-dicom"
    vol = imageio.volread(dir_path, "DICOM")

en donde ``vol`` es un tensor compatible con la librería NumPy. También podemos acceder a las dimensiones del tensor con el atributo ``shape`` y al tipo de dato del tensor con el atributo ``dtype``:

.. code-block:: python

    vol.shape    # (99, 512, 512)
    vol.dtype    # dtype('int16')

En este caso, se trata de un tensor de tipo ``int16``. Es decir, cada elemento del tensor es un entero. Por otro lado, las dimensiones del tensor son ``(99, 512, 512)``, es decir, se trata de una secuencia de ``99`` cortes de ``512x512``. 

Otro de los atributos importantes de una imagen es el número de canales. Cuando se genera un volumen con archivos DICOM, se recomienda incluir el número de canales en el tamaño del volumen. Como resultado, las dimensiones de un volumen siguen el formato ``(C, D, H, W)`` o ``(Channels, Depth, Height, Width)``. En el ejemplo anterior, cada corte es una matriz de ``512x512`` de un solo canal (i.e., está en escala de grises). Por lo tanto, las dimensiones del volumen son ``(1, 99, 512, 512)``.

La siguiente figura muestra seis de los cortes de la tomografía generados a partir del ejemplo anterior, es decir, con ``imageio.volread(dir_path, "DICOM")``. Cada corte es una imagen en escala de grises (un solo canal) de ``512x512``. Cuando se combinan todos los cortes, se obtiene un volumen o tensor con estas dimensiones, ``(1, 99, 512, 512)``.

.. figure:: images/fig_volume.jpg

    Cortes de una tomografía. Cada imagen representa un corte de la tomografía. Cada corte es una imagen en escala de grises de ``512x512``. Cuando se combinan los ``99`` cortes, se obtiene un volumen o tensor con las siguientes dimensiones, ``(1, 99, 512, 512)``, es decir, una secuencia de ``99`` imágenes de ``512x512`` de un solo canal (escala de grises).

La librería pydicom es más flexible que imageio, ya que nos brinda mayor control para leer los cortes y generar el volumen. Por esta razón, emplearemos esta librería de aquí en adelante. El siguiente ejemplo muestra cómo leer un archivo DICOM con pydicom. La función ``apply_modality_lut()`` permite convertir los valores de intensidad a `unidades de Hounsfield <https://pydicom.github.io/pydicom/dev/old/working_with_pixel_data.html>`_ (HU, `Hounsfield Units`). De este modo, el rango de valores cambia de ``[0, 4095]`` a ``[-1024, 3071]``. 

.. code-block:: python

    # Lectura de archivo DICOM con pydicom
    from pydicom import dcmread
    from pydicom.pixel_data_handlers.util import apply_modality_lut
    import matplotlib.pyplot as plt
    import numpy as np

    filepath = "/media/data/datasets/volumetric-dicom/000000.dcm"   # DICOM file
    dicom_image = dcmread(filepath)                                 # read DICOM file
    img_t = dicom_image.pixel_array                                 # tensor
    img_hu_t = apply_modality_lut(img_t, dicom_image)               # tensor in Hounsfield units

    print(img_t.shape)      # (512, 512)
    print(img_t.min())      # 0       
    print(img_t.max())      # 4095

    print(img_hu_t.shape)   # (512, 512)
    print(img_hu_t.min())   # -1024.0      
    print(img_hu_t.max())   #  3071.0

La escala de Hounsfield nos pemirte identificar tejidos y materiales en una imagen médica. Por ejemplo, el aire se encuentra cerca de -1,000 HU y el agua cerca de 0 HU [Stevens20]_. Como parte del preprocesamiento de las imágenes, algunos autores [Stevens20]_ recomiendan limitar el rango a ``[-1000, 1000]``:

`Some CT scanners use HU values that correspond to negative densities to indicate that those voxels are outside of the CT scanner's field of view. For our purposes, everything outside the patient should be air, so we discard that field-of-view information by setting a lower bound of the values to -1,000 HU. Similarly, the exact densities of bones, metal implants, and so on are not relevant to our case, so we cap density at roughly 2 g/cc (1,000 HU) even though that's not biologically accurate in most cases.`

Lo anterior equivale a la siguiente línea en PyTorch, donde la variable ``ct_a`` es un tensor:

.. code-block:: python

  ct_a.clip(-1000, 1000, ct_a)


Organización de las imágenes
----------------------------

Las imágenes facilitadas por el INR están organizadas en varias partes (directorios) de la siguiente manera:

.. code-block:: none

    inr_dicom/
        p1/
        p2/
        p3/
        ...

Cada directorio contiene un conjunto de archivos DICOM, generalmente tomografías. Por ejemplo, el directorio ``p2`` contiene la tomografía ``CT2`` que se compone de 138 archivos DICOM. Cada archivo DICOM representa un corte distinto. La tomografía ``CT2`` está organizada de la siguiente manera:

.. code-block:: none

    inr_dicom/
        p2/
            20190228/
                CT2/
                    1anonima
                    2anonima
                    ...
                    137anonima
                    138anonima

en donde ``inr_dicom``, ``p2``, ``20190228`` y ``CT2`` son directorios, mientras que ``1anonima..138anonima`` son 138 archivos en formato DICOM. Los archivos no tienen extensión.

Algunos modelos de segmentación, tal como en U-Net [Stevens20]_ [Ronneberger15]_, permiten segmentar imágenes médicas. Este modelo considera imágenes con las siguientes dimensiones, ``(1, D, 512, 512)``, esto es, una secuencia de ``D`` imágenes de ``512x512`` de un solo canal. Recomendamos que las imágenes médicas tengan esas dimensiones para entrenar el modelo de detección de objetos.

La siguiente figura muestra algunas de las imágenes DICOM que no tienen las dimensiones esperadas por el modelo de detección de objetos. La primera fila contiene imágenes a color (i.e., tres canales, RGB) de lo que parece ser un modelo 3D de huesos. La segunda fila contiene imágenes de diferentes partes del cuerpo (como piernas y pecho). La cuarta fila muestra imágenes que no son partes del cuerpo, sino que contiene datos del paciente en forma de texto. La última fila contiene imágenes que son considerablemente más largas que el resto de las imágenes del conjunto. 

.. figure:: images/fig_invalid_images.jpg

    Imágenes no válidas. Estas imágenes no son válidas, ya que sus dimensiones no son las esperadas por el modelo, porque son a color o porque contienen texto.

La siguiente figura muestra algunas de las imágenes que tienen las dimensiones esperadas por el modelo de detección de objetos: ``(1, D, 512, 512)``. La primera fila muestra imágenes con dos masas de un tamaño similar. La segunda fila muestra imágenes en donde una de las masas es mayor que la otra. En la tercera fila, la última imagen contiene una sola masa. Note que en la mayoría de los casos, las imágenes están delimitadas por una circunferencia. La última fila contiene imágenes de diverso tipo. 

.. figure:: images/fig_valid_images.jpg

    Imágenes con las dimensiones esperadas. La primer fila muestra imágenes con dos masas circulares. La segunda fila muestra imágenes en donde una de las dos masas es mayor a la otra. La última fila muestra otro tipo de imágenes.

    
Referencias
-----------

.. [Stevens20] Deep Learning with PyTorch. Eli Stevens, Luca Antiga, and Thomas Viehmann. Manning, 2020.

.. [Ronneberger15] U-Net: Convolutional Networks for Biomedical Image Segmentation. Olaf Ronneberger, Philipp Fischer, and Thomas Brox. 2015.