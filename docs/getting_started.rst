Guía de inicio rápido
=====================


Código
------

El código fuente de la aplicación está disponible en GitLab:

`https://gitlab.com/auraham/inr_object_detector <https://gitlab.com/auraham/inr_object_detector>`_. 



Generar documentación
---------------------

El código fuente de la documentación está disponible en GitLab:

`https://gitlab.com/auraham/inr_documentation <https://gitlab.com/auraham/inr_documentation>`_. 

Utilice los siguientes comandos para descargar el repositorio y generar la documentación:

.. code-block:: bash

    $ git clone git@gitlab.com:auraham/inr_documentation.git
    $ cd inr_documentation
    $ pip install -r requirements.txt
    $ cd docs
    $ make html                     

Para ver la documentación en un navegador, utilice los siguientes comandos:

.. code-block:: text

    $ cd _build/html
    $ python3 -m http.server 9000 --bind 127.0.0.1

La documentación estará disponible en ``http://localhost:9000``. También se puede generar la documentación en PDF de la siguiente manera:

.. code-block:: bash

    $ sudo apt-get install latexmk
    $ cd docs
    $ make latexpdf      
 
El archivo generado se encuentra en ``docs/_build/latex``.




