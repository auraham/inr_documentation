Guías para el desarrollador
===========================

Descargar imágenes con ``syncdownload``
---------------------------------------

``syncdownload`` es una aplicación desarrollada por Dante Domizzi para descargar las imágenes facilitadas por el INR de manera segura.
El código fuente de la aplicación está disponible en:

`http://disys1.tamps.cinvestav.mx:9090/domizzi/syncdownload <http://disys1.tamps.cinvestav.mx:9090/domizzi/syncdownload>`_. 


**Nota** Para utilizar el cliente, debe contactar al administrador (Dante) para crear una cuenta y agregarlo al grupo en donde se encuentran las imágenes. Además, debe contar con Java 8 o superior.


Utilice el siguiente comando para descargar el repositorio:


.. code-block:: bash

    $ git clone http://disys1.tamps.cinvestav.mx:9090/domizzi/syncdownload

Enseguida, edite el archivo ``PATH.txt`` para indicar el directorio en donde se descargarán las imágenes.

Para descargar las imágenes, ejecute el siguiente comando:

.. code-block:: bash

    $ java -jar SincronizadorTermDown.jar

Deberá ingresar el nombre de usuario y contraseña. Si los datos son correctos, las imágenes se descargarán en el directorio especificado en ``PATH.txt``.

