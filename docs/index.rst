.. Object Detection documentation master file, created by
   sphinx-quickstart on Thu May 27 16:06:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación - INR Detección de Objetos
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

   about
   getting_started
   developer_guides
   intro_to_dicom
   object_detection_guide
   


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
