Guía para entrenar el modelo de detección de objetos
====================================================

Esta guía describe cómo entrenar el modelo de detección de objetos usando la `API de detección de objetos de TensorFlow 2 <https://github.com/tensorflow/models/tree/master/research/object_detection>`_.
Para más detalles, consulte la documentación [Vladimirov]_.

El procedimiento general es el siguiente:

- Preparar el entorno de trabajo.
- Preparar las imágenes.
- Generar TF Records a partir de las imágenes.
- Descargar un modelo pre-entrenado.
- Configurar el `pipeline` de entrenamiento.
- Entrenar el modelo.
- Exportar el modelo.
- Probar el modelo.

.. note::

    En esta guía se descargarán varios repositorios Git. Por convención, los repositorios se encontrarán en ``~/git``.


.. note::
    
    Esta guía se probó en Ubuntu 20.04:

    .. code-block:: bash

        [custom_object_detector] auraham@rocket ~ $ cat /etc/os-release
        NAME="Ubuntu"
        VERSION="20.04.2 LTS (Focal Fossa)"
        ID=ubuntu
        ID_LIKE=debian
        PRETTY_NAME="Ubuntu 20.04.2 LTS"
        VERSION_ID="20.04"
        HOME_URL="https://www.ubuntu.com/"
        SUPPORT_URL="https://help.ubuntu.com/"
        BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
        PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
        VERSION_CODENAME=focal
        UBUNTU_CODENAME=focal





.. TODO 
.. X Crear repo `inr_object_detector`
.. X Crear `requirements.txt` en `inr_object_detector`
.. Crear todos los directorios del workspace para mostrarlos en la seccion Directorios del entorno de trabajo
.. Copiar la estructura de `tf_object_detection_api` en `inr_object_detector`
.. Los directorios vacios debe llevar un README.md para poder alojarlos en gitlab
.. Actualizar estructura en la seccion 'Preparar el entorno de trabajo'
.. Hacer notebook para crear la imagen con las 30 imagenes de entrenamiendo del INR
.. Hacer imagen con dos subplots: a) imagen de test con el bounding box real y b) con el bounding box obtenido
.. Repetir lo anterior con las otras dos imagenes de test para el reporte.
.. Cambiar los comandos de git clone para usar una direccion http en lugar de ssh


Preparar el entorno de trabajo
------------------------------

Para facilitar el entrenamiento del modelo de detección de objetos, organizaremos el proyecto de la siguiente manera:

..  Actualizar este bloque

.. code-block:: bash

    ~/git/tf_object_detection_api $ tree -L 2
    .
    ├── models
    │   ├── AUTHORS
    │   ├── CODEOWNERS
    │   ├── community
    │   ├── CONTRIBUTING.md
    │   ├── ISSUES.md
    │   ├── LICENSE
    │   ├── official
    │   ├── orbit
    │   ├── README.md
    │   └── research
    ├── README.md
    ├── scripts
    │   └── preprocessing
    └── workspace
        └── training_demo

El repositorio ``inr_object_detector`` contiene gran parte de la estructura anterior. Otros elementos como la API de detección de objetos de TensorFlow y los modelos pre-enternados deben ser descargados por separado. El repositorio ``inr_object_detector`` está disponible en:

`https://gitlab.com/auraham/inr_object_detector <https://gitlab.com/auraham/inr_object_detector>`_. 

Utilice los siguientes comandos para descargar el repositorio:

.. code-block:: bash

    $ cd ~/git
    $ git clone https://gitlab.com/auraham/inr_object_detector.git


Instalar TensorFlow 2
*********************

Cree un entorno virtual para instalar las dependencias:

.. code-block:: bash

    $ mkvirtualenv custom_object_detector -p python3.8

Instale TensorFlow 2 y las dependencias dentro del entorno virtual ``custom_object_detector``:

.. code-block:: bash

    $ workon custom_object_detector
    $ cd ~/git/inr_object_detector
    $ pip install -r requirements.txt

Para verificar la instalación, escriba lo siguiente en ``ipython``:

.. code-block:: python

    import tensorflow as tf
    tf.__version__

La salida debe ser similar a la siguiente:

.. TODO: Cambiar a tensorflow==2.5.0

.. code-block:: none

    (custom_object_detector) auraham@rocket:~/git/inr_object_detector$ ipython
    Python 3.8.5 (default, May 27 2021, 13:30:53) 
    Type 'copyright', 'credits' or 'license' for more information
    IPython 7.24.1 -- An enhanced Interactive Python. Type '?' for help.

    In [1]: import tensorflow as tf
    2021-06-15 11:24:15.439124: I tensorflow/stream_executor/platform/default/dso_loader.cc:49] Successfully opened dynamic library libcudart.so.11.0
    t
    In [2]: tf.__version__
    Out[2]: '2.4.1'


Instalar TensorFlow Object Detection API 
****************************************

La API de detección de objetos de TensorFlow debe instalarse a partir del `código fuente en GitHub <https://github.com/tensorflow/models>`_.

Utilice los siguientes comandos para descargar el repositorio:

.. code-block:: bash

    $ cd ~/git
    $ git clone https://github.com/tensorflow/models.git

Copie el contenido del repositorio ``models`` en ``inr_object_detector/models`` de la siguiente manera:

.. code-block:: bash

    $ cd ~/git/inr_object_detector
    $ mkdir models
    $ cd models
    $ rsync -azP --exclude '.git' --exclude '.gitignore' --exclude '.github' ~/git/models/ .  # no olvide el . al final 

El directorio ``models`` debe lucir de la siguiente manera:

.. code-block:: bash

    ~/git/inr_object_detector/models $ tree -L 1
    .
    ├── AUTHORS
    ├── CODEOWNERS
    ├── community
    ├── CONTRIBUTING.md
    ├── ISSUES.md
    ├── LICENSE
    ├── official
    ├── orbit
    ├── README.md
    └── research

    4 directories, 6 files

La API de detección de objetos de TensorFlow utiliza `Protocol Buffers <https://developers.google.com/protocol-buffers/>`_  para configurar el modelo y los parámetros del entrenamiento. Ejecute estos comandos para compilar archivos e instalar la API:

.. code-block:: bash

    $ sudo apt-get install protobuf-compiler  # instalar protoc
    $ cd ~/git/inr_object_detector/models/research
    $ protoc object_detection/protos/*.proto --python_out=.
    $ cp object_detection/packages/tf2/setup.py .
    $ pip install .

Para verificar que el paquete ``object_detection`` se ha copiado en su en el entorno virtual ``custom_object_detector``, ejecute lo siguiente:

.. code-block:: bash

    $ workon custom_object_detector
    $ cd ~
    $ ipython

Enseguida, escriba lo siguiente:

.. code-block:: python

    import object_detection
    object_detection.__path__
    
La salida debe ser similar a la siguiente:
    
.. code-block:: python

    In [1]: import object_detection

    In [2]: object_detection.__path__
    Out[2]: ['/home/auraham/.virtualenvs/custom_object_detector/lib/python3.8/site-packages/object_detection']


Finalmente, ejecute este comando para verificar la instalación:

.. code-block:: bash

    $ cd ~/git/inr_object_detector/models/research
    $ python object_detection/builders/model_builder_tf2_test.py

La salida debe ser similar a la siguiente:

.. code-block:: none

    I0618 07:26:11.222657 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_create_ssd_models_from_config): 19.22s
    [       OK ] ModelBuilderTF2Test.test_create_ssd_models_from_config
    [ RUN      ] ModelBuilderTF2Test.test_invalid_faster_rcnn_batchnorm_update
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_invalid_faster_rcnn_batchnorm_update): 0.01s
    I0618 07:26:11.232823 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_invalid_faster_rcnn_batchnorm_update): 0.01s
    [       OK ] ModelBuilderTF2Test.test_invalid_faster_rcnn_batchnorm_update
    [ RUN      ] ModelBuilderTF2Test.test_invalid_first_stage_nms_iou_threshold
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_invalid_first_stage_nms_iou_threshold): 0.0s
    I0618 07:26:11.234459 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_invalid_first_stage_nms_iou_threshold): 0.0s
    [       OK ] ModelBuilderTF2Test.test_invalid_first_stage_nms_iou_threshold
    [ RUN      ] ModelBuilderTF2Test.test_invalid_model_config_proto
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_invalid_model_config_proto): 0.0s
    I0618 07:26:11.234906 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_invalid_model_config_proto): 0.0s
    [       OK ] ModelBuilderTF2Test.test_invalid_model_config_proto
    [ RUN      ] ModelBuilderTF2Test.test_invalid_second_stage_batch_size
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_invalid_second_stage_batch_size): 0.0s
    I0618 07:26:11.236236 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_invalid_second_stage_batch_size): 0.0s
    [       OK ] ModelBuilderTF2Test.test_invalid_second_stage_batch_size
    [ RUN      ] ModelBuilderTF2Test.test_session
    [  SKIPPED ] ModelBuilderTF2Test.test_session
    [ RUN      ] ModelBuilderTF2Test.test_unknown_faster_rcnn_feature_extractor
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_unknown_faster_rcnn_feature_extractor): 0.0s
    I0618 07:26:11.237448 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_unknown_faster_rcnn_feature_extractor): 0.0s
    [       OK ] ModelBuilderTF2Test.test_unknown_faster_rcnn_feature_extractor
    [ RUN      ] ModelBuilderTF2Test.test_unknown_meta_architecture
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_unknown_meta_architecture): 0.0s
    I0618 07:26:11.237817 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_unknown_meta_architecture): 0.0s
    [       OK ] ModelBuilderTF2Test.test_unknown_meta_architecture
    [ RUN      ] ModelBuilderTF2Test.test_unknown_ssd_feature_extractor
    INFO:tensorflow:time(__main__.ModelBuilderTF2Test.test_unknown_ssd_feature_extractor): 0.0s
    I0618 07:26:11.238688 140127712278336 test_util.py:2102] time(__main__.ModelBuilderTF2Test.test_unknown_ssd_feature_extractor): 0.0s
    [       OK ] ModelBuilderTF2Test.test_unknown_ssd_feature_extractor
    ----------------------------------------------------------------------
    Ran 24 tests in 23.535s

    OK (skipped=1)


Directorios del entorno de trabajo
****************************************

El directorio ``workspace/training_demo`` contiene el entorno de trabajo. La estructura es la siguiente:

.. Actualizar bloque

.. code-block:: bash

    ~/git/inr_object_detector/workspace/training_demo $ tree -L 2
    .
    ├── annotations
    │   ├── label_map.pbtxt
    │   ├── test.record
    │   └── train.record
    ├── exported-models
    │   └── centernet_hg104_512x512_coco17_tpu-8
    ├── exported_models
    │   └── centernet_hg104_512x512_coco17_tpu-8
    ├── exporter_main_v2.py
    ├── images
    │   ├── test
    │   └── train
    ├── model_main_tf2.py
    ├── models
    │   ├── centernet_hg104_512x512_coco17_tpu-8
    │   ├── centernet_resnet101_v1_fpn_512x512_coco17_tpu-8
    │   ├── efficientdet_d0_coco17_tpu-32
    │   └── efficientdet_d4_coco17_tpu-32
    ├── notebooks
    │   ├── inference_from_saved_model_tf2_colab.ipynb
    │   ├── inference_tf2_colab.ipynb
    │   └── test_saved_model.ipynb
    ├── out.jpg
    ├── output
    │   └── paciente2_114.jpg
    ├── output.jpg
    ├── plot_object_detection_checkpoint.ipynb
    ├── plot_object_detection_checkpoint.py
    ├── pretrained_models
    │   ├── centernet_hg104_512x512_coco17_tpu-8
    │   ├── centernet_hg104_512x512_coco17_tpu-8.tar.gz
    │   ├── centernet_resnet101_v1_fpn_512x512_coco17_tpu-8
    │   ├── centernet_resnet101_v1_fpn_512x512_coco17_tpu-8.tar.gz
    │   ├── efficientdet_d0_coco17_tpu-32
    │   ├── efficientdet_d0_coco17_tpu-32.tar.gz
    │   ├── efficientdet_d4_coco17_tpu-32
    │   └── efficientdet_d4_coco17_tpu-32.tar.gz
    ├── streamlit_app.py
    ├── streamlit_app.py.bak
    └── streamlit_app.works.py


A continuación se muestra una breve explicación de los archivos dentro del directorio.

======================= ===============================================================================
Nombre                  Descripción
======================= ===============================================================================
``images``              | Contiene dos conjuntos de imágenes organizados en directorios distintos:
                        | ``train`` contiene las imágenes para entrenar el modelo y ``test`` contiene 
                        | las imágenes para probar el modelo. Cada imagen ``image.jpg`` está  
                        | acompañada por un archivo con anotaciones ``image.xml``.
``annotations``         | Contiene los siguientes archivos: ``label_map.pbtxt``, que define los nombres
                        | de las clases; ``test.record`` y ``train.record`` son archivos TF Record, que
                        | representan a las imagenes de entrenamiento y de prueba en un formato
                        | binario.
``pretrained_models``   | Contiene los modelos pre-entrenados del `Model Zoo <https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md>`_.
``models``              | Contiene el modelo entrenado a partir de las imágenes de ``images``.
``exported_models``     | Luego del entrenamiento, podemos exportar el modelo entrenado para
                        | portarlo a otra plataforma. Este directorio contiene el modelo exportado.
``notebooks``           | Contiene notebooks de Jupyter para mostrar el funcionamiento del modelo.
``model_main_tf2.py``   | Script principal para entrenar el modelo.
``test_saved_model.py`` | Script para probar el modelo.
``streamlit_app.py``    | Aplicación web realizada con `Streamlit <https://streamlit.io/>`_ para probar el modelo.
======================= ===============================================================================


Preparar las imágenes
---------------------

En esta sección se describe cómo etiquetar las imágenes así como la creación del conjunto de entrenamiento y del conjunto de prueba.

Etiquetar imágenes
******************

En esta guía, las imágenes se encuentran en ``/media/data/datasets/inr_dicom/images``:

.. code-block:: bash

    /media/data/datasets/inr_dicom/images $ ls *.jpg
    paciente1_23.jpg  paciente1_42.jpg  paciente1_46.jpg   paciente2_112.jpg  paciente3_19.jpg  paciente3_85.jpg  paciente3_89.jpg  paciente3_93.jpg
    paciente1_39.jpg  paciente1_43.jpg  paciente1_47.jpg   paciente2_113.jpg  paciente3_82.jpg  paciente3_86.jpg  paciente3_90.jpg
    paciente1_40.jpg  paciente1_44.jpg  paciente2_110.jpg  paciente2_114.jpg  paciente3_83.jpg  paciente3_87.jpg  paciente3_91.jpg
    paciente1_41.jpg  paciente1_45.jpg  paciente2_111.jpg  paciente2_20.jpg   paciente3_84.jpg  paciente3_88.jpg  paciente3_92.jpg

Al momento de escribir esta guía, contamos con 29 imágenes:

.. code-block:: bash

    /media/data/datasets/inr_dicom/images $ ls *.jpg -1 | wc -l
    29



.. note::
    
    Al momento de escribir esta guía, las imágenes se encuentran en archivos ``zip``:
    
    .. code-block:: none

        /media/data/datasets/inr_dicom $ ls paciente*.zip
        paciente1.zip  paciente2.zip  paciente3.zip

    Cada archivo ``zip`` contiene tomografías en formato DICOM. Puede utilizar el script ``unzip_and_merge_images.py`` en el directorio ``inr_object_detector/scripts/preprocessing`` para descomprimir los archivos ``zip``, convertir las imágenes de formato DICOM a formato JPG, renombrar las imágenes y concentrarlas en un solo directorio. Para esto, ejecute lo siguiente:

    .. code-block:: none

        $ cd ~/git/inr_object_detector/scripts/preprocessing
        $ python unzip_and_merge_images.py \
            -i /media/data/datasets/inr_dicom \
            -o /media/data/datasets/inr_dicom/images

    donde:

    - ``i`` indica el directorio en donde se encuentran las imágenes en formato ``zip``.
    - ``o`` indica el directorio en donde se descomprimiran las imágenes.

    Al terminar el script, las imágenes ``jpg`` se encontrarán en ``/media/data/datasets/inr_dicom/images``.

Utilizaremos el programa ``labelImg`` para etiquetar las imágenes. Este programa está disponible en `GitHub <https://github.com/tzutalin/labelImg>`_. Ejecute el siguiente comando para instalarlo:

.. code-block:: bash

    $ pip install labelimg

Enseguida, ejecute los siguientes comandos para etiquetar las imágenes:

.. code-block:: none

    $ cd /media/data/datasets/inr_dicom/images
    $ labelImg .

La siguiente figura muestra el programa ``labelImg`` y una tomografía para ilustrar el etiquetado de las imágenes. La región de interés está delimitada por un rectángulo o `bounding box` de color azul. Cada tomografía debe ser etiquetada manualmente por un médico especialista.

.. figure:: images/labelimg.png

    Programa ``labelImg`` emplado para etiquetar una tomografía.

En el ejemplo anterior, se muestra la imagen ``paciente1_42.jpg``. Luego de etiquetar las regiones de interés, las anotaciones se guardan en un archivo XML. En este caso, las anotaciones se encuentran en ``paciente1_42.xml``:

.. code-block:: bash

    /media/data/datasets/inr_dicom/images $ cat paciente1_42.xml
    
.. code-block:: xml

    <annotation>
        <folder>images</folder>
        <filename>paciente1_42.jpg</filename>
        <path>/media/data/datasets/inr_dicom/images/paciente1_42.jpg</path>
        <source>
            <database>Unknown</database>
        </source>
        <size>
            <width>512</width>
            <height>512</height>
            <depth>1</depth>
        </size>
        <segmented>0</segmented>
        <object>
            <name>masa</name>
            <pose>Unspecified</pose>
            <truncated>0</truncated>
            <difficult>0</difficult>
            <bndbox>
                <xmin>153</xmin>
                <ymin>234</ymin>
                <xmax>229</xmax>
                <ymax>310</ymax>
            </bndbox>
        </object>
    </annotation>


Cada *bounding box* tiene los siguientes atributos:

- ``name`` es el nombre de la clase del *bounding box*. En el ejemplo anterior, la región de interés pertenece a la clase ``masa``. En caso de existir más de una clase, como ``maligno`` o ``benigno``, debe especificarse al momento de anotar la imagen con ``labelImg``.
- ``bndbox`` contiene las coordenadas del rectángulo, es decir, ``xmin``, ``xmax``, ``ymin`` y ``ymax``.


Los nombres de las clases son asignados por el usuario (e.g., el médico especialista) y deben ser definidos en el archivo ``label_map.pbtxt`` dentro del directorio ``annotations``. En esta guía hemos definido una sola clase llamada ``masa`` de la siguiente manera:

.. code-block:: python

    item {
        id: 1,
        name: 'masa'
    }


.. note::

    Es importante que los nombres de las clases definidos en ``label_map.pbtxt`` y en las anotaciones XML sea iguales. Por ejemplo, no puede usar ``masa`` en ``label_map.pbtxt`` y ``mass`` en ``paciente1_42.xml`` para referirse a la misma clase.


Luego de etiquetar las imágenes, el directorio ``images`` debe lucir de la siguiente manera:


.. code-block:: bash

    /media/data/datasets/inr_dicom/images $ tree 
    .
    ├── paciente1_23.jpg
    ├── paciente1_23.xml
    ├── paciente1_39.jpg
    ├── paciente1_39.xml
    ├── paciente1_40.jpg
    ├── paciente1_40.xml
    ├── paciente1_41.jpg
    ├── paciente1_41.xml
    ├── paciente1_42.jpg
    ├── paciente1_42.xml
    ├── paciente1_43.jpg
    ├── paciente1_43.xml
    ├── paciente1_44.jpg
    ├── paciente1_44.xml
    ├── paciente1_45.jpg
    ├── paciente1_45.xml
    ├── paciente1_46.jpg
    ├── paciente1_46.xml
    ├── paciente1_47.jpg
    ├── paciente1_47.xml
    ├── paciente2_110.jpg
    ├── paciente2_110.xml
    ├── paciente2_111.jpg
    ├── paciente2_111.xml
    ├── paciente2_112.jpg
    ├── paciente2_112.xml
    ├── paciente2_113.jpg
    ├── paciente2_113.xml
    ├── paciente2_114.jpg
    ├── paciente2_114.xml
    ├── paciente2_20.jpg
    ├── paciente2_20.xml
    ├── paciente3_19.jpg
    ├── paciente3_19.xml
    ├── paciente3_82.jpg
    ├── paciente3_82.xml
    ├── paciente3_83.jpg
    ├── paciente3_83.xml
    ├── paciente3_84.jpg
    ├── paciente3_84.xml
    ├── paciente3_85.jpg
    ├── paciente3_85.xml
    ├── paciente3_86.jpg
    ├── paciente3_86.xml
    ├── paciente3_87.jpg
    ├── paciente3_87.xml
    ├── paciente3_88.jpg
    ├── paciente3_88.xml
    ├── paciente3_89.jpg
    ├── paciente3_89.xml
    ├── paciente3_90.jpg
    ├── paciente3_90.xml
    ├── paciente3_91.jpg
    ├── paciente3_91.xml
    ├── paciente3_92.jpg
    ├── paciente3_92.xml
    ├── paciente3_93.jpg
    ├── paciente3_93.xml
    └── README.md

    0 directories, 59 files


.. note::

    Se ha creado un `repositorio en GitLab <https://gitlab.com/auraham/images>`_ con las etiquetas de las 29 imágenes disponibles al momento de escribir esta guía. Ejecute el siguiente comando para descargar el repositorio. Este repositorio sólo contiene anotaciones, no contiene imágenes:

    .. code-block:: none

        $ git clone https://gitlab.com/auraham/images.git


Creación del conjunto de entrenamiento y del conjunto de prueba
***************************************************************

El conjunto de imágenes se encuentra en ``/media/data/datasets/inr_dicom/images`` y se compone de archivos JPG y de anotaciones XML. Se recomienda dividir el conjunto de imágenes en dos conjuntos:

- Conjunto de entrenamiento ``train``: Utilizamos este conjunto para entrenar el modelo, es decir, para ajustar los párametros (pesos) del modelo.

- Conjunto de prueba ``test``: Utilizamos este conjunto para evaluar el desempeño del modelo utilizando imágenes que no hayan sido empleadas durante el entrenamiento (i.e., que el modelo no haya `visto`).

Puede utilizar el script ``partition_dataset.py`` para dividir el conjunto de imágenes de la siguiente manera:

.. code-block:: none

    $ cd ~/git/inr_object_detector/scripts/preprocessing
    $ python partition_dataset.py \
        -x \
        -r 0.1 \
        -i input_dir \
        -o output_dir

El script ``partition_dataset.py`` acepta los siguientes parámetros:

- ``x`` es una bandera para copiar los archivos XML junto con las imágenes JPG.
- ``r`` define la proporción de imágenes de prueba. Por ejemplo, ``-r 0.1`` indica que se usará el 10% del total de imágenes como imágenes de prueba.
- ``i`` es el directorio de entrada en donde se encuentran las imágenes (archivos JPG y XML).
- ``o`` es el directorio de salida en donde se copiarán las imágenes de entrenamiento y de prueba.

Por ejemplo, para dividir las imágenes del directorio ``/media/data/datasets/inr_dicom/images`` y copiarlas al directorio ``~/git/inr_object_detector/workspace/training_demo/images`` utilice estos comandos:

.. code-block:: none

    $ cd ~/git/inr_object_detector/scripts/preprocessing
    $ python partition_dataset.py \
        -x \
        -r 0.1 \
        -i /media/data/datasets/inr_dicom/images \
        -o /home/auraham/git/inr_object_detector/workspace/training_demo/images


Luego de dividir el conjunto de imágenes, el directorio ``images`` debe lucir así:

.. code-block:: none
 
    ~/git/inr_object_detector/workspace/training_demo/images $ tree
    .
    ├── README.md
    ├── test
    │   ├── paciente1_42.jpg
    │   ├── paciente1_42.xml
    │   ├── paciente2_114.jpg
    │   ├── paciente2_114.xml
    │   ├── paciente3_88.jpg
    │   └── paciente3_88.xml
    └── train
        ├── paciente1_23.jpg
        ├── paciente1_23.xml
        ├── paciente1_39.jpg
        ├── paciente1_39.xml
        ├── paciente1_40.jpg
        ├── paciente1_40.xml
        ├── paciente1_41.jpg
        ├── paciente1_41.xml
        ├── paciente1_43.jpg
        ├── paciente1_43.xml
        ├── paciente1_44.jpg
        ├── paciente1_44.xml
        ├── paciente1_45.jpg
        ├── paciente1_45.xml
        ├── paciente1_46.jpg
        ├── paciente1_46.xml
        ├── paciente1_47.jpg
        ├── paciente1_47.xml
        ├── paciente2_110.jpg
        ├── paciente2_110.xml
        ├── paciente2_111.jpg
        ├── paciente2_111.xml
        ├── paciente2_112.jpg
        ├── paciente2_112.xml
        ├── paciente2_113.jpg
        ├── paciente2_113.xml
        ├── paciente2_20.jpg
        ├── paciente2_20.xml
        ├── paciente3_19.jpg
        ├── paciente3_19.xml
        ├── paciente3_82.jpg
        ├── paciente3_82.xml
        ├── paciente3_83.jpg
        ├── paciente3_83.xml
        ├── paciente3_84.jpg
        ├── paciente3_84.xml
        ├── paciente3_85.jpg
        ├── paciente3_85.xml
        ├── paciente3_86.jpg
        ├── paciente3_86.xml
        ├── paciente3_87.jpg
        ├── paciente3_87.xml
        ├── paciente3_89.jpg
        ├── paciente3_89.xml
        ├── paciente3_90.jpg
        ├── paciente3_90.xml
        ├── paciente3_91.jpg
        ├── paciente3_91.xml
        ├── paciente3_92.jpg
        ├── paciente3_92.xml
        ├── paciente3_93.jpg
        └── paciente3_93.xml

    2 directories, 59 files


Generar TF Records a partir de las imágenes
-------------------------------------------

Los modelos de la API de detección de objetos de TensorFlow utilizan TF Records. En lugar de usar imágenes en formato JPG, se recomienda emplear el formato TF Record.

Utilice el script ``generate_tfrecord.py`` para convertir imágenes al formato TF Record. El script se encuentra en ``~/git/inr_object_detector/scripts/preprocessing``.

Ejemplo:

.. code-block:: none

    $ cd ~/git/inr_object_detector/scripts/preprocessing
    $ python generate_tfrecord.py \
        -x /path/to/images/train \
        -l /path/to/annotations/label_map.pbtxt \
        -o /path/to/annotations/train.record


El script ``generate_tfrecord.py`` acepta los siguientes paramétros:

- ``x`` especifica el directorio en donde se encuentran las anotaciones XML.
- ``l`` especifica el directorio en donde se encuentra el archivo ``label_map.pbtxt``.
- ``o`` especifica el directorio en donde se guardarán los archivos TF Record (extensión `.record`).

El siguiente comando convierte las imágenes del conjunto de entrenamiento en ``train``:

.. code-block:: bash

    $ python generate_tfrecord.py \
        -x /media/data/git/inr_object_detector/workspace/training_demo/images/train \
        -l /media/data/git/inr_object_detector/workspace/training_demo/annotations/label_map.pbtxt \
        -o /media/data/git/inr_object_detector/workspace/training_demo/annotations/train.record

El siguiente comando convierte las imágenes del conjunto de prueba en ``test``:

.. code-block:: bash

    $ python generate_tfrecord.py \
        -x /media/data/git/inr_object_detector/workspace/training_demo/images/test \
        -l /media/data/git/inr_object_detector/workspace/training_demo/annotations/label_map.pbtxt \
        -o /media/data/git/inr_object_detector/workspace/training_demo/annotations/test.record    


La salida es:

.. code-block:: bash

    Successfully created the TFRecord file: /media/data/git/inr_object_detector/workspace/training_demo/annotations/train.record

    Successfully created the TFRecord file: /media/data/git/inr_object_detector/workspace/training_demo/annotations/test.record


Descargar un modelo pre-entrenado
---------------------------------

La API de detección de objetos de TensorFlow ofrece varios modelos pre-entredados en el llamado `Model Zoo <https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md>`_.

En esta guía utilizaremos el modelo `CenterNet HourGlass104 512X512 <http://download.tensorflow.org/models/object_detection/tf2/20200713/centernet_hg104_512x512_coco17_tpu-8.tar.gz>`_. Ejecute los siguientes comandos para descargar el modelo en el entorno de trabajo:

.. code-block:: none

    $ cd ~/git/inr_object_detector/workspace/training_demo/pretrained_models
    $ wget http://download.tensorflow.org/models/object_detection/tf2/20200713/centernet_hg104_512x512_coco17_tpu-8.tar.gz


Enseguida, descomprima el modelo:

.. code-block:: bash

    $ tar xzvf centernet_hg104_512x512_coco17_tpu-8.tar.gz


El entorno de trabajo debe lucir de la siguiente manera:


.. code-block:: bash

    ~/git/inr_object_detector/workspace/training_demo/pretrained_models $ tree -L 3
    .
    ├── centernet_hg104_512x512_coco17_tpu-8
    │   ├── checkpoint
    │   │   ├── checkpoint
    │   │   ├── ckpt-0.data-00000-of-00001
    │   │   └── ckpt-0.index
    │   ├── pipeline.config
    │   └── saved_model
    │       ├── assets
    │       ├── saved_model.pb
    │       └── variables
    ├── centernet_hg104_512x512_coco17_tpu-8.tar.gz
    └── README.md

    5 directories, 7 files

De estos archivos, necesitaremos el archivo de configuración ``pipeline.config`` en la siguiente sección. Puede repetir el proceso anterior para descargar más modelos en ``pretrained_models``.


Configurar el `pipeline` de entrenamiento
-----------------------------------------

La API de detección de objetos de TensorFlow utiliza un archivo llamado ``pipeline.config`` para configurar el entrenamiento del modelo. La documentación sobre las opciones de configuración se encuentra disponible en `GitHub <https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/configuring_jobs.md>`_.

.. note::

    El repositorio ``inr_object_detector`` contiene archivos ``pipeline.config`` con los parámetros recomendados de diferentes modelos, por lo que puede omitir esta sección.

Cada modelo pre-entrenado viene acompañado de un archivo de configuración ``pipeline.config``. A continuación describimos cómo editar el archivo correspondiente del modelo `CenterNet HourGlass104 512X512 <http://download.tensorflow.org/models/object_detection/tf2/20200713/centernet_hg104_512x512_coco17_tpu-8.tar.gz>`_.

Cree un nuevo directorio en ``models`` y copie el archivo de configuración del modelo:

.. code-block:: none

    $ cd ~/git/inr_object_detector/workspace/training_demo/models
    $ mkdir centernet_hg104_512x512_coco17_tpu-8
    $ cd centernet_hg104_512x512_coco17_tpu-8
    $ cp ../../pretrained_models/centernet_hg104_512x512_coco17_tpu-8/pipeline.config .


El entorno de trabajo debe lucir de la siguiente manera:

.. code-block:: bash

    ~/git/inr_object_detector/workspace/training_demo/models $ tree -L 3
    .
    └── centernet_hg104_512x512_coco17_tpu-8
        └── pipeline.config


Ahora, edite ``pipeline.config`` como se muestra en los comentarios:

.. literalinclude:: code/pipeline.config
    :language: Python


Entrenar el modelo
------------------

El script ``model_main_tf2.py`` permite entrenar el modelo de detección de objetos. Este script se encuentra en:

.. code-block:: bash

    ~/git/inr_object_detector/models/research/object_detection/model_main_tf2.py

Para facilitar el entrenamiento, hay una copia de este script en el entorno de trabajo.

Inicie el proceso de entramiento de la siguiente manera:

.. code-block:: none

    $ cd ~/git/inr_object_detector/workspace/training_demo
    $ python model_main_tf2.py \
        --model_dir=models/centernet_hg104_512x512_coco17_tpu-8 \
        --pipeline_config_path=models/centernet_hg104_512x512_coco17_tpu-8/pipeline.config

.. AGREGAR SALIDA DE STOUT (NO USES EL REPO ``tf_object_detection``)

Puede utilizar los siguientes comandos para iniciar TensorBoard y así revisar el proceso de entrenamiento desde un navegador en `localhost:6006 <http://localhost:6006>`_:

.. code-block:: none

    $ cd ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8
    $ tensorboard --logdir . --bind_all

.. note::

    Para acceder a TensorBoard desde el servidor ``stout`` (``stout.tamps.cinvestav.mx``), realice lo siguiente:

    - Utilice la VPN de CINVESTAV:

    .. code-block:: none
        
        $ sudo openvpn --config config_file.ovpn

    - Inicie TensorBoard en el servidor:

    .. code-block:: none

        $ cd ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8
        $ tensorboard --logdir . --bind_all

    - Acceda a  `stout.tamps.cinvestav.mx:6006 <stout.tamps.cinvestav.mx:6006>`_. 


Exportar el modelo
------------------

Al terminar el proceso de entrenamiento, se habrán generado los siguientes archivos dentro del directorio del modelo:

.. code-block:: none

    ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8 $ tree -L 2
    .
    ├── checkpoint
    ├── ckpt-STEP_NUMBER.data-00000-of-00001
    ├── ckpt-STEP_NUMBER.index
    ├── pipeline.config
    └── train
        └── events.out.tfevents.1623184546.stouttampscinvestavmx.2612361.2197.v2


Encontrará varios `checkpoints`, es decir, archivos que contienen los parámetros del modelo de una iteración especiífica (``STEP_NUMBER``) del proceso de entrenamiento. 
Un `checkpoint` se compone de dos archivos con extensión ``data-00000-of-00001`` y ``.index``.
Por ejemplo, el siguiente bloque muestra los últimos seis `checkpoints` del modelo `CenterNet HourGlass104 512X512 <http://download.tensorflow.org/models/object_detection/tf2/20200713/centernet_hg104_512x512_coco17_tpu-8.tar.gz>`_ que corresponden a las iteraciones 107 a 112:

.. code-block:: none

    ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8 $ tree -L 1
    .
    ├── checkpoint
    ├── ckpt-107.data-00000-of-00001
    ├── ckpt-107.index
    ├── ckpt-108.data-00000-of-00001
    ├── ckpt-108.index
    ├── ckpt-109.data-00000-of-00001
    ├── ckpt-109.index
    ├── ckpt-110.data-00000-of-00001
    ├── ckpt-110.index
    ├── ckpt-111.data-00000-of-00001
    ├── ckpt-111.index
    ├── ckpt-112.data-00000-of-00001
    ├── ckpt-112.index

El archivo ``checkpoint`` es un archivo de texto que contiene información adicional sobre los `checkpoints`. 
    

.. code-block:: none

    ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8 $ cat checkpoint

.. code-block:: none

    model_checkpoint_path: "ckpt-112"
    all_model_checkpoint_paths: "ckpt-107"
    all_model_checkpoint_paths: "ckpt-108"
    all_model_checkpoint_paths: "ckpt-109"
    all_model_checkpoint_paths: "ckpt-110"
    all_model_checkpoint_paths: "ckpt-111"
    all_model_checkpoint_paths: "ckpt-112"
    all_model_checkpoint_timestamps: 1623215590.9863546
    all_model_checkpoint_timestamps: 1623215882.8594937
    all_model_checkpoint_timestamps: 1623216174.9149847
    all_model_checkpoint_timestamps: 1623216465.3351288
    all_model_checkpoint_timestamps: 1623216756.5147283
    all_model_checkpoint_timestamps: 1623217052.0542383
    all_model_checkpoint_timestamps: 1623217344.5619946
    last_preserved_timestamp: 1623184555.8963132

Los `checkpoints` son puntos de restauración del modelo. Podemos utilizar estos puntos de restauración para:

- Cargar el modelo, es decir, generar un grafo de TensorFlow a partir de los parámetros del `checkpoint`.
- Exportar el modelo, es decir, generar una versión reducida del modelo a partir de los paramétros del `checkpoint`.

En ambos casos se requiere un `checkpoint` (archivos con extension ``data-00000-of-00001`` y ``.index``) y el archivo de texto ``checkpoints``.


.. note::

    La primera línea del archivo ``checkpoints``:

    .. code-block:: none

        model_checkpoint_path: "ckpt-112"

    indica el nombre del punto de restauración. Si se desea cargar o exportar algún otro punto de restauración, por ejemplo, el correspondiente a la iteración 110, edite el archivo  ``checkpoints`` de este modo:

    .. code-block:: none

        model_checkpoint_path: "ckpt-110"

A continuación mostraremos cómo exportar un punto de restauración. En este caso, usaremos ``STEP_NUMBER=112``. Ejecute los siguientes comandos para descargar el punto de restauración:


.. code-block:: none

    $ cd ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8
    $ rsync -azP acamacho@stout:/home/acamacho/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8/checkpoint .
    $ rsync -azP acamacho@stout:/home/acamacho/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8/ckpt-112.index .
    $ rsync -azP acamacho@stout:/home/acamacho/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8/ckpt-112.data-00000-of-00001 .


.. note::

    El tamaño de los archivos ``.data-00000-of-00001`` suelen ser de 2 GB.


Ejecute los siguientes comandos para exportar el modelo:

.. code-block:: none

    $ cd ~/git/inr_object_detector/workspace/training_demo
    $ python exporter_main_v2.py \
        --input_type image_tensor \
        --pipeline_config_path models/centernet_hg104_512x512_coco17_tpu-8/pipeline.config \
        --trained_checkpoint_dir models/centernet_hg104_512x512_coco17_tpu-8 \
        --output_directory exported_models/centernet_hg104_512x512_coco17_tpu-8


.. note::

    Puede encontrarse con este error al ejecutar el script ``exporter_main_v2.py``:


    .. code-block:: none

        tensorflow.python.framework.errors_impl.NotFoundError: Could not find checkpoint or SavedModel at models/centernet_hg104_512x512_coco17_tpu-8/ckpt-STEP_NUMBER.

    Este error ocurre al intentar restaurar un `checkpoint` distinto al indicado en ``checkpoint``. Considere el siguiente ejemplo. El directorio ``centernet_hg104_512x512_coco17_tpu`` contiene un solo punto de restauración que corresponde al ``STEP_NUMBER=112``:

    .. code-block:: none

        ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8 $ tree -L 1
        .
        ├── checkpoint
        ├── ckpt-112.data-00000-of-00001
        ├── ckpt-112.index
        ├── pipeline.config
        └── train
        
        1 directory, 4 files

    Ese mismo valor debe aparecer en la primera línea de ``checkpoint``. En caso contrario, generará el error anterior:

    .. code-block:: none

        ~/git/inr_object_detector/workspace/training_demo/models/centernet_hg104_512x512_coco17_tpu-8 $ head -n 1 checkpoint 
        model_checkpoint_path: "ckpt-112"
    
    
    Si desea probar un punto de restauración distinto, descárguelo del servidor y edite ``checkpoint`` para indicar el nombre del punto de restauración.

    
El script ``exporter_main_v2.py`` acepta los siguientes parámetros:

- ``input_type`` especifica que la entrada del modelo es un tensor (tipo ``unit8`` de shape ``[1, None, None, 3]``).
- ``pipeline_config_path``  indica la ubicación del archivo ``pipeline.config`` del modelo.
- ``trained_checkpoint_dir``  indica la ubicación del punto de restauración.
- ``output_directory`` indica la ubicación para exportar el modelo.

La salida debe ser similar a la siguiente:

.. code-block:: none

    ~/git/inr_object_detector/workspace/training_demo/exported_models/centernet_hg104_512x512_coco17_tpu-8 $ tree -L 3
    .
    ├── checkpoint
    │   ├── checkpoint
    │   ├── ckpt-0.data-00000-of-00001
    │   └── ckpt-0.index
    ├── pipeline.config
    └── saved_model
        ├── assets
        ├── saved_model.pb
        └── variables
            ├── variables.data-00000-of-00001
            └── variables.index

    4 directories, 7 files


.. note::

    Es importante que la versión de TensorFlow usada para ejecutar ``model_main_tf2.py`` sea la misma para ejecutar el script ``exporter_main_v2.py``. En otras palabras, se recomienda usar la misma versión de TensorFlow para entrenar, exportar y probar el modelo. En otro caso, se producirá un error. En esta guía se ha usado ``tensorflow==2.5.0`` en el servidor ``stout`` y en una laptop.


Probar el modelo
----------------

Ejecute el notebook ``test_saved_model.ipynb`` para probar el modelo. Este script usará el modelo ubicado en el directorio ``exported_models`` y detectará objetos en las imágenes de prueba del directorio ``images/test``:


.. code-block::

    $ cd ~/git/inr_object_detector/workspace/training_demo/notebooks
    $ jupyter notebook

También puede ejecutar el script ``test_saved_model.py``:

.. code-block::

    $ cd ~/git/inr_object_detector/workspace/training_demo
    $ python test_saved_model.py


Luego de probar el modelo, se mostrarán las siguientes imágenes:


.. figure:: images/paciente1_42.jpg

    Imagen ``paciente1_42.jpg``.

.. figure:: images/paciente2_114.jpg

    Imagen ``paciente2_114.jpg``.

.. figure:: images/paciente3_88.jpg

    Imagen ``paciente3_88.jpg``.



Referencias
-----------


.. [Vladimirov] `Training Custom Object Detector. <https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html>`_

